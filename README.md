## PUBG DATA INTAKE - Innovation project

```
cd kafka-docker
docker-compose up -d
```
Docker compose will start relatively a kafka broker cluster,
a zookeeper instance to manage the kafka cluster and a PostgreSQL database,
with exposed port 5432, with default username and specified password in the 
docker compose. 

In order to prepare the DB use psql or your favourite DB 
operator and run the following SQL:
```SQL
CREATE ROLE kafka_stream_user WITH PASSWORD 'postgres' SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN;

CREATE DATABASE pubg_data_intake OWNER postgres;

CONNECT pubg_data_intake kafka_stream_user;


CREATE TABLE pubg_data_intake (
    "id" text PRIMARY KEY,
    "json_values" jsonb
    );

SELECT * FROM pubg_data_intake;
```

In order to run the application using the pre-trained data model 
simply run 
```sbt run```

To follow the kafka messages input use the following command 
in your kafka /bin directory:
```./kafka-console-consumer --bootstrap-server localhost:9092 --topic gameanalysis6 --from-beginning```