package modules

import com.google.inject.AbstractModule
import play.api.{Configuration, Environment}
import play.api.inject.Binding
import services.{KafkaSparkStreaming, KafkaStreamCSVIntake}

class InitModule extends AbstractModule {
  override def configure() = {

    bind(classOf[KafkaStreamCSVIntake]).asEagerSingleton()

    bind(classOf[KafkaSparkStreaming]).asEagerSingleton()
  }

}