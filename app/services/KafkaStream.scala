package services

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}

object KafkaStream {

  lazy val spark = SparkSession.builder().master("local").appName("Innovation project").getOrCreate()

  val mySchema = StructType(Array(
    StructField("Id", StringType),
    StructField("groupId", StringType),
    StructField("matchId", StringType),
    StructField("assists", DoubleType),
    StructField("boosts", StringType),
    StructField("damageDealt", DoubleType),
    StructField("DBNOs", StringType),
    StructField("headshotKills", StringType),
    StructField("heals", StringType),
    StructField("killPlace", DoubleType),
    StructField("killPoints", DoubleType),
    StructField("kills", DoubleType),
    StructField("killStreaks", DoubleType),
    StructField("longestKill", StringType),
    StructField("matchDuration", StringType),
    StructField("matchType", StringType),
    StructField("maxPlace", StringType),
    StructField("numGroups", StringType),
    StructField("rankPoints", StringType),
    StructField("revives", StringType),
    StructField("rideDistance", StringType),
    StructField("roadKills", StringType),
    StructField("swimDistance", StringType),
    StructField("teamKills", StringType),
    StructField("vehicleDestroys", StringType),
    StructField("walkDistance", StringType),
    StructField("weaponsAcquired", StringType),
    StructField("winPoints", StringType)
  ))

  val topic = "gameanalysis7"

}
