package services

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{Normalizer, StringIndexer, VectorAssembler}
import org.apache.spark.ml.regression.{GBTRegressor, LinearRegression}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}

object SparkCSVModelTraining extends App {

  val spark = SparkSession.builder().master("local").appName("Innovation project").getOrCreate()

  val mySchema = StructType(Array(
    StructField("Id", StringType),
    StructField("groupId", StringType),
    StructField("matchId", StringType),
    StructField("assists", DoubleType),
    StructField("boosts", StringType),
    StructField("damageDealt", DoubleType),
    StructField("DBNOs", StringType),
    StructField("headshotKills", StringType),
    StructField("heals", StringType),
    StructField("killPlace", DoubleType),
    StructField("killPoints", DoubleType),
    StructField("kills", DoubleType),
    StructField("killStreaks", DoubleType),
    StructField("longestKill", StringType),
    StructField("matchDuration", StringType),
    StructField("matchType", StringType),
    StructField("maxPlace", StringType),
    StructField("numGroups", StringType),
    StructField("rankPoints", StringType),
    StructField("revives", StringType),
    StructField("rideDistance", StringType),
    StructField("roadKills", StringType),
    StructField("swimDistance", StringType),
    StructField("teamKills", StringType),
    StructField("vehicleDestroys", StringType),
    StructField("walkDistance", StringType),
    StructField("weaponsAcquired", StringType),
    StructField("winPoints", StringType)
  ))

  val dataFrame = spark.read.option("header", true).schema(mySchema).csv("resources/train_V2.csv").na.drop()

  print(dataFrame.show(2))
  val Array(trainingData2, testData) = dataFrame.randomSplit(Array(0.7, 0.3))
  val trainingData = trainingData2
  val labelColumn = "killPlace"

//  val idIndexer = new StringIndexer().setInputCol("Id").setOutputCol("IdIndexer").setHandleInvalid("skip")

  val assembler = new VectorAssembler()
    .setInputCols(Array("kills", "damageDealt", "assists"))
    .setOutputCol("features")
    .transform(trainingData)

  val assembler2 = new VectorAssembler()
    .setInputCols(Array("kills", "damageDealt", "assists"))
    .setOutputCol("features")
    .transform(testData)

//  val normalizer = new Normalizer()
//    .setInputCol("features")
//    .setOutputCol("normFeatures")
//    .setP(2.0)
//    .transform(assembler)
//
//  val normalizer2 = new Normalizer()
//    .setInputCol("features")
//    .setOutputCol("normFeatures")
//    .setP(2.0)
//    .transform(assembler2)

  val lr = new LogisticRegression()
    .setLabelCol(labelColumn)
    .setPredictionCol("Predicted " + labelColumn)
    .setFeaturesCol("features")
    .setMaxIter(10)
    .setRegParam(1.0)
    .setElasticNetParam(1.0)

  val gbt = new GBTRegressor()
    .setLabelCol(labelColumn)
    .setFeaturesCol("features")
    .setPredictionCol("Predicted " + labelColumn)
    .setMaxIter(10)


  val stages = Array(gbt)

  val pipeline = new Pipeline().setStages(stages)
  val model = pipeline.fit(assembler)
  val predictions = model.transform(assembler2)

  val evaluator = new RegressionEvaluator()
    .setLabelCol(labelColumn)
    .setPredictionCol("Predicted " + labelColumn)
    .setMetricName("rmse")

  val error = evaluator.evaluate(predictions)
  println(error)
  model.write.overwrite().save("spark-model/")
  println(predictions.select("Id", "features", "killPlace", "Predicted killPlace").show(20))
  spark.stop()

}
