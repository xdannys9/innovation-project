package services
import java.io.Serializable

import com.google.inject.{Inject, Singleton}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.{ForeachWriter, Row}
import org.apache.spark.sql.streaming.ProcessingTime
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import anorm._
import play.api.db._

class KafkaSparkStreaming @Inject()(db: Database){

  val spark = KafkaStream.spark
  val schema = KafkaStream.mySchema

  import spark.implicits._
  val url="jdbc:postgresql://localhost/pubg_data_intake"
  val user="kafka_stream_user"
  val pwd="postgres"
  val writer = new JDBCSink(url, user, pwd)

    val df =
      spark.readStream
        .format("kafka")
        .option("kafka.bootstrap.servers", "localhost:9092")
        .option("subscribe", KafkaStream.topic)
        .option("startingOffsets", "earliest")
        .option("enable.auto.commit", (false: java.lang.Boolean))
        .load()

  val data = df
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
    .where($"key".isNotNull)
    .as[(String, String)]
//  val dataset = data.writeStream.format("console").start()
//  dataset.awaitTermination()

//  val query = data.writeStream.foreach{new ForeachWriter[(String, String)] extends Serializable {
//
//         def open(partitionId: Long, version: Long): Boolean = {
//             // open connection
//           true
//           }
//
//         def process(value: (String, String)) = {
//           // write string to connection
//
//           println(value)
//           db.withConnection { implicit connection: java.sql.Connection =>
//             SQL(
//               """
//                 |INSERT INTO pubg_data_intake(id, json_values)
//                 |VALUES ({value1}, to_json({value2}::jsonb));
//               """).on(
//               "value1" -> value._1,
//               "value2" -> value._2
//             ).executeUpdate()
//           }
//         }
//
//         def close(errorOrNull: Throwable): Unit = {
//             // close the connection
//           }
//       }}
//    .start()

  val query = data.writeStream.foreach(writer).start()

  query.awaitTermination()






//  val kafkaParams = Map[String, Object](
//    "bootstrap.servers" -> "localhost:9092",
//    "key.deserializer" -> classOf[StringDeserializer],
//    // used as i am using a string serializer for the input kafka topic
//    "value.deserializer" -> classOf[StringDeserializer],
//    "group.id" -> "spark-demo",
//    "kafka.consumer.id" -> "kafka-consumer-01"
//  )
//
//  val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
//
//
//  val inputStream = KafkaUtils.createDirectStream(ssc,
//    PreferConsistent, Subscribe[String, String](Array(KafkaStream.topic), kafkaParams))
//
//  println("HERE")
//  val processedStream = inputStream.map{ record =>
//    println(record)
//    record.value}
//
//  //checking the batches for data
//  processedStream.print()

}
