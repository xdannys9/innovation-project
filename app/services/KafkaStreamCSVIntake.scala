package services

import com.google.inject.Singleton
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.{ForeachWriter, SparkSession}
import org.apache.spark.sql.types._

class KafkaStreamCSVIntake {

  val spark = KafkaStream.spark
  val schema = KafkaStream.mySchema
  val pipeline = PipelineModel.read.load("spark-model")

  val data = spark.readStream.schema(schema).csv("resources/test/").na.drop()

  val assembler = new VectorAssembler()
    .setInputCols(Array("kills", "damageDealt", "assists"))
    .setOutputCol("features")
    .transform(data)

  pipeline
    .transform(assembler)
    .select("kills", "damageDealt", "assists",  "Id", "Predicted killPlace")
    .selectExpr("CAST(Id AS STRING) AS key", "to_json(struct(*)) AS value")
    .writeStream
    .format("kafka")
    .option("topic", KafkaStream.topic)
    .option("kafka.bootstrap.servers", "localhost:9092")
    .option("checkpointLocation", "tmp/")
//    .option("enable.auto.commit", (false: java.lang.Boolean))
    .start()
}

object KafkaStreamCSVIntake extends App {

  val spark = SparkSession.builder().master("local").appName("Innovation project").getOrCreate()
  import spark.implicits._

  val schema = KafkaStream.mySchema
  val pipeline = PipelineModel.read.load("spark-model")

  val data = spark.readStream.schema(schema).csv("resources/test/").na.drop()

  val assembler = new VectorAssembler()
    .setInputCols(Array("kills", "damageDealt", "assists"))
    .setOutputCol("features")
    .transform(data)

  val query = pipeline
    .transform(assembler)
    .select("kills", "damageDealt", "assists",  "Id", "Predicted killPlace")
    .selectExpr("CAST(Id AS STRING) AS key", "to_json(struct(*)) AS value")
    .as[(String, String)]
    .writeStream
    .foreach{new ForeachWriter[(String, String)]{

             def open(partitionId: Long, version: Long): Boolean = {
                 // open connection
               true
               }

             def process(value: (String, String)) = {
               // write string to connection

               println(value)

             }

             def close(errorOrNull: Throwable): Unit = {
                 // close the connection
               }
           }}
    .start()

    query.awaitTermination()

}