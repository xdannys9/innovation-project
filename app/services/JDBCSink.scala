package services

import anorm._

class JDBCSink(url: String, user:String, pwd:String) extends org.apache.spark.sql.ForeachWriter[(String, String)]{
  val driver = "org.postgresql.Driver"
  var connection:java.sql.Connection = _
  var statement:java.sql.Statement = _

  def open(partitionId: Long, version: Long):Boolean = {
    Class.forName(driver)
    connection = java.sql.DriverManager.getConnection(url, user, pwd)
    statement = connection.createStatement
    true
  }

  def process(value :(String, String)): Unit = {
    print(value)
    SQL("""
          INSERT INTO pubg_data_intake(id, json_values) VALUES ({value1}, to_json({value2}::jsonb));
    """).on(
      "value1" -> value._1,
      "value2" -> value._2
    ).executeUpdate()(connection)
  }

  def close(errorOrNull:Throwable):Unit = {
    connection.close
  }
}