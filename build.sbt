name := """pubg-data-intake"""
organization := "fi.metropolia"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.12"

dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.7"
dependencyOverrides +=  "com.google.guava" % "guava" % "15.0"

libraryDependencies ++= Seq(
  guice,
  jdbc,
  "com.typesafe.play" %% "anorm" % "2.5.1",
  "org.postgresql" % "postgresql" % "42.2.5",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.apache.kafka" %% "kafka" % "2.0.0",
//  "org.apache.spark" %% "spark-streaming-kafka" % "1.6.3",
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.3.2",
  "org.apache.spark" % "spark-core_2.11" % "2.3.2",
  "org.apache.spark" % "spark-sql_2.11" % "2.3.2",
  "org.apache.spark" % "spark-mllib_2.11" % "2.3.2",
  "org.apache.spark" % "spark-sql-kafka-0-10_2.11" % "2.3.2",
  "org.apache.kafka" % "kafka-clients" % "0.11.0.1")
